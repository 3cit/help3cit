# HELP, 3cIT!

Uma aplicação destinada a atendimentos, solução de problemas e dúvidas que venham ser enviadas pelo usuário.

## Arquivo de configuração

```env
# Settings file
ALLOWED_HOSTS=<array: hosts_permitidos>
DEVELOPMENT_MODE=<bool: modo_desenv. default: False>
OS3CDB_NAME=<nome_do_banco>
OS3CDB_USER=<usuario_do_banco>
OS3CDB_PASSWORD=<senha_do_banco>
OS3CDB_HOST=<endereco_ou_ip_do_servidor_do_banco>
TEST_OS3CDB_NAME=<nome_do_banco_(p/_testes)>
SECRET_KEY=<chave_secreta(p/_geracao_das_senhas)>
EMAIL_HOST=<endereco_servidor_smtp_(email)>
EMAIL_PORT=<porta_servidor_smtp>
EMAIL_HOST_USER=<usuario_servidor_smtp>
EMAIL_HOST_PASSWORD=<senha_usuario_servidor_smtp>
USE_TLS=<bool: usar_tls? Default: True>
USE_SSH=<bool: usar_ssh? Default: True>
DEFAULT_FROM_EMAIL=<email_padrao_a_ser_colocado_como_from:_caso_não_tenha_sido_informado_um>
DEFAULT_SAC_EMAIL=<email_atendimento_ao_consumidor_padrao>
RUN_SELENIUM_TESTS=<bool: rodar_testes_selenium?>
```
