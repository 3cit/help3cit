from django.conf import settings


def project(request):
    """
    Retorna o nome do projeto, utilizado para identificação de páginas e
    e-mails enviados pela aplicação.
    """
    project_name = ''
    if hasattr(settings, 'PROJECT_NAME'):
        project_name = settings.PROJECT_NAME
    return dict(project_name=project_name)
