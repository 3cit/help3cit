from rest_framework import permissions


class IsItSelfOrReadOnly(permissions.BasePermission):
    """
    Custom permission to verify if user is itself, a staff member or an admin
    """

    def has_object_permission(self, request, view, obj):
        """
        Verify if object to get/change is an instance of request user or if request.user is something important.
        """
        if request.method in permissions.SAFE_METHODS and request.user.is_staff:
            return True

        # Admin can
        if request.user.is_superuser or request.user.is_staff:
            if hasattr(obj, 'is_staff'):
                if obj.is_staff and not request.user.is_superuser:
                    return False
            return True

        # Only the admin can delete an user
        if request.method == 'DELETE' and not request.user.is_superuser or not request.user.is_staff:
            return False

        return request.user.id == obj.id


class IsAdminOrStaffMember(permissions.BasePermission):
    """
    ...
    """

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser or request.user.is_staff:
            return True
        return False
