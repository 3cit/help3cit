import { createBrowserHistory } from "history";
import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import {
  //BrowserRouter as Router,
  Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { transitions, positions, Provider as AlertProvider } from "react-alert";

import store from "../redux/store";
import PrivateRoute from "./security/PrivateRoute";
import { authenticateUser } from "../redux/actions/autenticacao";

import Header from "./layout/Header";
import Alert from "./layout/Alert";
import CustomAlertTemplate from "./layout/CustomAlertTemplate";
import Dashboard from "./ordem-servico/Dashboard";
//import OrdemServicoList from "./ordem-servico/OrdemServicoList";
//import OrdemServicoForm from "./ordem-servico/OrdemServicoForm";
//import OrdemServicoDetail from "./ordem-servico/OrdemServicoDetail";
import LandingPage from "./autenticacao/LandingPage";
import ErrorBoundary from "./erros/ErrorBoundary";
import Http404 from "./erros/Http404";
import AlteracaoDadosForm from "./usuario/AlteracaoDadosForm";

const alertOptions = {
  position: positions.TOP_RIGHT,
  timeout: 3000,
  transition: transitions.FADE,
  template: CustomAlertTemplate
};

const browserHistory = createBrowserHistory({
  basename: "app/"
});

class App extends Component {
  componentDidMount = () => {
    store.dispatch(authenticateUser());
    console.log(this.props);
    // TODO: Resolver problema ao acessar URLs manualmente.
    //console.log(this.props.history);
    //console.log(document.location.pathname);
  };

  render() {
    const { host } = window.location;
    return (
      <Provider store={store}>
        <AlertProvider {...alertOptions}>
          <Router history={browserHistory}>
            <ErrorBoundary>
              <Alert />
              <Header />
              <section className="section">
                <div className="container">
                  <Switch>
                    {/*
                      TODO: Puxar as rotas de /os a partir do dashboard
                      <PrivateRoute exact path="/os" component={Dashboard} />
                    */}
                    <Route exact path="/" component={LandingPage} />
                    <PrivateRoute path="/os/" component={Dashboard} />
                    <PrivateRoute
                      path="/usuario/alterar-dados"
                      component={AlteracaoDadosForm}
                    />
                    <Route component={Http404} />
                  </Switch>
                </div>
              </section>
            </ErrorBoundary>
          </Router>
        </AlertProvider>
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root-app"));
