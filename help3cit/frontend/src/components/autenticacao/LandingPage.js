import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import propTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class LandingPage extends Component {
  static propTypes = {
    autenticacao: propTypes.object.isRequired
  };

  render() {
    if (this.props.autenticacao.isAuthenticated) {
      return <Redirect to="/os/" />;
    }

    return (
      <div>
        <h1 className="title is-1">Suporte 3cIT</h1>
        <div className="content">
          <p>
            Bem vindo a página de suporte dos produtos e serviços desempenhados
            pela 3cIT.
          </p>
          <p>
            Para utilizar este sistema, é necessário ter uma conta em um dos
            serviços da 3cIT. Com isso, apenas informe os mesmos dados nos
            campos no canto superior direito desta página para fazer uma nova
            solicitação, reclamação, informe ou enviar seu feedback sobre o uso
            ou usufruto de um de nossos serviços ou ver as ações já feitas.
          </p>
          <ul>
            <li>
              <a href="#">Sobre este app</a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  autenticacao: state.autenticacao
});

export default connect(mapStateToProps)(LandingPage);
