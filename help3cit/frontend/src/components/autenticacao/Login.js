import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { logUserIn } from "../../redux/actions/autenticacao";

class Login extends React.Component {
  static propTypes = {
    autenticacao: PropTypes.object.isRequired,
    logUserIn: PropTypes.func.isRequired
  };

  state = {
    username: "",
    password: "",
    isSubmitting: false
  };

  handleChange = event => {
    let newValue = event.target.value;
    this.setState({ [event.target.name]: newValue });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ isSubmitting: true });
    const { username, password } = this.state;
    this.props.logUserIn({ username, password });
    this.setState({ isSubmitting: false });
  };

  render() {
    const { username, password, isSubmitting } = this.state;
    return (
      <form type="POST" onSubmit={this.handleSubmit}>
        <div className="field is-horizontal">
          <div className="field-label is-small">
            <label htmlFor="username">Usuário/Senha:</label>
          </div>
          <div className="field-body">
            <div className="field">
              <div className="control">
                <input
                  type="text"
                  className="input is-small"
                  value={username}
                  name="username"
                  id="username"
                  placeholder="Nome de usuário"
                  onChange={this.handleChange}
                  disabled={isSubmitting}
                  autoFocus
                  required
                />
              </div>
            </div>
            <div className="field">
              <div className="control">
                <input
                  type="password"
                  className="input is-small"
                  value={password}
                  name="password"
                  id="password"
                  placeholder="Senha"
                  onChange={this.handleChange}
                  disabled={isSubmitting}
                  required
                />
              </div>
            </div>
          </div>
          <div className="field">
            <div className="control">
              <button
                className={
                  isSubmitting
                    ? "button is-primary is-loading is-small"
                    : "button is-primary is-small"
                }
                type="submit"
              >
                Entrar
              </button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  autenticacao: state.autenticacao
});

export default connect(
  mapStateToProps,
  { logUserIn }
)(Login);
