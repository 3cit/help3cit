import React, { Component } from "react";
import PropTypes from "prop-types";

export class Http404 extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired
  };

  render() {
    const { history } = this.props;
    return (
      <div>
        <h2 className="hero title">Nada encontrado...</h2>
        <div className="content">
          <p>
            A página que você procura não existe. Verifique o link clicado ou a
            URL informada.
          </p>
          <ul>
            <li>
              Se você estiver vendo esta página novamente após clicar no mesmo
              link, você pode optar por abrir um chamado para este problema.
            </li>
          </ul>
        </div>
        <a onClick={history.goBack}>Voltar à página anterior</a>
      </div>
    );
  }
}

export default Http404;
