import React, { Component, Fragment } from "react";
import { withAlert } from "react-alert";
import { connect } from "react-redux";
import PropTypes from "prop-types";

export class Alert extends Component {
  static propTypes = {
    alert: PropTypes.object.isRequired,
    message: PropTypes.object.isRequired
  };

  componentDidUpdate(prevProps) {
    const { message, alert } = this.props;
    if (message !== prevProps.message) {
      alert.show(message.message, { type: message.level });
    }
  }

  render() {
    return <Fragment />;
  }
}

const mapStateToProps = state => ({
  message: state.messages
});

export default connect(mapStateToProps)(withAlert()(Alert));
