import React from "react";
import { types } from "react-alert";

const getIcon = type => {
  switch (type) {
    case types.ERROR:
      return <i className="fa fa-lg fa-exclamation has-text-light" />;
    case types.SUCCESS:
      return <i className="fa fa-lg fa-check has-text-light" />;
    case types.INFO:
    default:
      return <i className="fa fa-lg fa-info has-text-light" />;
  }
};

const CustomAlertTemplate = ({ message, options, style, close }) => (
  <div
    className={`notification is-${
      options.type === "error" ? "danger" : options.type
    }`}
    style={style}
  >
    <button className="delete" onClick={close} />
    <span className="icon is-medium">{getIcon(options.type)}</span>
    {message}
  </div>
);

export default CustomAlertTemplate;
