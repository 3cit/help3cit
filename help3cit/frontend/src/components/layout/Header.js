import React, { Component, Fragment } from "react";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import Login from "../autenticacao/Login";

import { logUserOut } from "../../redux/actions/autenticacao";

class Header extends Component {
  static propTypes = {
    autenticacao: PropTypes.object.isRequired,
    logUserOut: PropTypes.func.isRequired
  };

  toggleMenu = e => {
    let burgerButtonElement = e.currentTarget;
    let navbarElement = e.currentTarget.parentElement.parentElement;
    let navbarMenuElement = navbarElement.getElementsByClassName("navbar-menu");
    if (navbarMenuElement) {
      navbarMenuElement = navbarMenuElement[0];
    }
    burgerButtonElement.classList.toggle("is-active");
    navbarMenuElement.classList.toggle("is-active");
  };

  render() {
    return (
      <nav
        className="navbar is-primary is-fixed-top"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <div
            style={{
              backgroundImage: `url(${THRCIT_BRAND})`,
              backgroundOrigin: 0,
              backgroundRepeat: "no-repeat",
              backgroundSize: "100px 50px",
              width: "140px",
              height: "40px"
            }}
          />
          <a
            role="button"
            className="navbar-burger burger"
            aria-label="menu"
            aria-expanded="false"
            data-target="mainNavbarHelp3cit"
            onClick={this.toggleMenu}
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </a>
        </div>

        <div id="mainNavbarHelp3cit" className="navbar-menu">
          <div className="navbar-end">
            {this.props.autenticacao.isAuthenticated ? (
              <Fragment>
                <span className="navbar-item">
                  Olá, &nbsp;
                  {(this.props.autenticacao.user &&
                    this.props.autenticacao.user.first_name) || (
                    <em>anônimo</em>
                  )}
                  &nbsp; (
                  {this.props.autenticacao.user &&
                    this.props.autenticacao.user.email}
                  )
                </span>
                <Link className="navbar-item" to="/usuario/alterar-dados/">
                  Alterar dados
                </Link>
                <a onClick={this.props.logUserOut} className="navbar-item">
                  Sair
                </a>
              </Fragment>
            ) : (
              <div className="navbar-item">
                <div className="buttons">
                  <Login />
                </div>
              </div>
            )}
          </div>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  autenticacao: state.autenticacao
});

export default connect(
  mapStateToProps,
  { logUserOut }
)(Header);
