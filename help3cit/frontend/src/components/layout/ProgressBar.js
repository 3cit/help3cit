import React, { Component } from "react";
import PropTypes from "prop-types";

export class ProgressBar extends Component {
  static propTypes = {
    max: PropTypes.number,
    value: PropTypes.number,
    defaultText: PropTypes.string
  };

  state = {
    max: 100,
    value: null,
    defaultText: "Carregando..."
  };

  render() {
    const { max, value, defaultText } = this.state;
    if (value !== null) {
      return (
        <progress className="progress" max={max} value={value}>
          {`${value}/${max}...`}
        </progress>
      );
    }
    return (
      <div>
        <span>{defaultText}</span>
        <progress className="progress" max={max} />
      </div>
    );
  }
}

export default ProgressBar;
