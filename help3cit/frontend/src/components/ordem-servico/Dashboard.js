import React, { Component, Fragment } from "react";
import { Switch, Route, Link } from "react-router-dom";
import { connect } from "react-redux";
import PrivateRoute from "../security/PrivateRoute";
import PropTypes from "prop-types";
import OrdemServicoForm from "./OrdemServicoForm";
import OrdemServicoList from "./OrdemServicoList";
import OrdemServicoDetail from "./OrdemServicoDetail";

export class Dashboard extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired,
    match: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  render() {
    const { user } = this.props;
    const { path, url } = this.props.match;
    return (
      <Fragment>
        <h2 className="title is-2">Solicitações</h2>
        <div className="columns">
          <div className="column is-two-fifths">
            <Switch>
              <PrivateRoute
                path={`${path}abrir`}
                component={OrdemServicoForm}
              />
              <PrivateRoute
                path={`${path}:numProtocolo`}
                component={OrdemServicoDetail}
              />
              <Route
                render={() => {
                  if (!user.is_staff) {
                    return (
                      <div className="content">
                        <ul>
                          <li>
                            Ao abrir um chamado relatando um problema, tente
                            recaptular quais foram as ações praticadas que
                            levaram ao aparecimento da anomalia;
                          </li>
                          <li>
                            Chamados do tipo "Sugestão" ou "Feedback" serão
                            automaticamente declarados como <em>resolvido</em>s.
                          </li>
                        </ul>
                        <Link
                          className="button is-primary"
                          to={`${path}abrir/`}
                        >
                          Abrir nova
                        </Link>
                      </div>
                    );
                  }
                  return (
                    <div>
                      <span className="is-size-5">
                        Olá, gestor/colaborador.
                      </span>
                      <div className="content">
                        <p>
                          Selecione uma solicitação para ver, realizar
                          atendimento ou declarar seu encerramento. Chamados do
                          tipo "Sugestão" ou "Feedback" serão automaticamente
                          declarados como
                          <em>resolvido</em>s.
                        </p>
                      </div>
                    </div>
                  );
                }}
              />
            </Switch>
          </div>
          <div className="column">
            <OrdemServicoList {...this.props} />
          </div>
        </div>
      </Fragment>
    );
  }
}

let mapStateToProps = state => ({
  user: state.autenticacao.user,
  isLoading: state.ordensServico.isLoading
});

export default connect(mapStateToProps)(Dashboard);
