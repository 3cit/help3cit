import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  marcarResolvidoOrdemServico,
  atenderOrdemServico,
  encerrarOrdemServico
} from "../../redux/actions/ordem-servico";
import ProgressBar from "../layout/ProgressBar";

const MarcarResolvidoButton = ({
  resolvido,
  user_is_staff,
  marcarResolvidoCallback
}) => {
  if (!resolvido && !user_is_staff) {
    return (
      <p className="control">
        <a className="button is-success" onClick={marcarResolvidoCallback}>
          Marcar como resolvido
        </a>
      </p>
    );
  }
  return <Fragment />;
};

const IniciarAtendimentoButton = ({
  resolvido,
  atendido,
  user_is_staff,
  onClickHandle
}) => {
  if (!resolvido && !atendido && user_is_staff) {
    return (
      <p className="control">
        <a className="button is-warning" onClick={onClickHandle}>
          Iniciar atendimento
        </a>
      </p>
    );
  }
  return <Fragment />;
};

const EncerrarAtendimentoButton = ({
  resolvido,
  atendido,
  encerrado,
  user_is_staff,
  onClickHandle
}) => {
  if (!resolvido && atendido && !encerrado && user_is_staff) {
    return (
      <p className="control">
        <a className="button is-success" onClick={onClickHandle}>
          Encerrar atendimento
        </a>
      </p>
    );
  }
  return <Fragment />;
};

export class OrdemServicoDetail extends Component {
  static propTypes = {
    usuario: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    ordensServico: PropTypes.array.isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        numProtocolo: PropTypes.string.isRequired
      })
    }),
    marcarResolvidoOrdemServico: PropTypes.func.isRequired,
    atenderOrdemServico: PropTypes.func.isRequired,
    encerrarOrdemServico: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired
  };

  _getOrdemServicoId = (protocolo, ordensServico) => {
    const { id } = ordensServico.find(os => os.protocolo === protocolo);
    return id;
  };

  marcarResolvido = () => {
    const { ordensServico, match } = this.props;
    let id = this._getOrdemServicoId(match.params.numProtocolo, ordensServico);
    this.props.marcarResolvidoOrdemServico(id);
  };

  iniciarAtendimento = () => {
    const { ordensServico } = this.props;
    const { numProtocolo } = this.props.match.params;
    let id = this._getOrdemServicoId(numProtocolo, ordensServico);
    console.log(id);
    this.props.atenderOrdemServico(id);
  };

  encerrarAtendimento = () => {
    const { ordensServico } = this.props;
    const { numProtocolo } = this.props.match.params;
    let id = this._getOrdemServicoId(numProtocolo, ordensServico);
    this.props.encerrarOrdemServico(id);
  };

  getPtBrDate = d => {
    if (typeof d === "string") d = new Date(d);
    return `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`;
  };

  render() {
    const {
      // Puxando direto para atualizar em tempo real.
      id,
      protocolo,
      produto_servico,
      assunto,
      resumo,
      detalhe_problema,
      datahora_abertura,
      datahora_atendimento,
      datahora_encerramento,
      atendente,
      requerente,
      resolvido
    } = this.props.ordensServico.find(
      os => os.protocolo === this.props.match.params.numProtocolo
    );
    const dt_abertura = new Date(datahora_abertura);
    const { usuario, history, isLoading } = this.props;

    if (isLoading) {
      return <ProgressBar />;
    }

    return (
      <Fragment>
        <h2 className="subtitle is-5">Chamado n. #{protocolo}</h2>
        <div className="columns">
          <div className="column is-one-third">
            <span className="label">Produto/Serviço</span>
            {produto_servico}
          </div>
          <div className="column">
            <span className="label">Resumo</span>
            {resumo}
          </div>
        </div>
        <span className="label">Detalhes</span>
        <div className="content">{detalhe_problema}</div>
        <br />
        <div className="columns">
          <div className="column is-two-third">
            {!usuario.is_staff ? (
              datahora_atendimento ? (
                <span>
                  {`Atendido por ${atendente} em ${this.getPtBrDate(
                    datahora_atendimento
                  )}`}
                </span>
              ) : (
                <span className="hs-text-warning">Ainda não atendido</span>
              )
            ) : (
              <span>
                {`Enviado por ${requerente || "anônimo"} em ${this.getPtBrDate(
                  dt_abertura
                )}`}
              </span>
            )}
          </div>
          <div className="column">
            {resolvido ? (
              <span className="has-text-success">
                {assunto} finalizado(a){" "}
                {datahora_encerramento
                  ? `em ${this.getPtBrDate(datahora_encerramento)}`
                  : ""}
                .
              </span>
            ) : (
              <span className="has-text-danger">
                {assunto} ainda não finalizado.
              </span>
            )}
          </div>
        </div>
        {/** anexo */}
        <div className="field is-grouped">
          <p className="control">
            <a className="button" onClick={history.goBack}>
              Voltar
            </a>
          </p>
          <MarcarResolvidoButton
            resolvido={resolvido}
            user_is_staff={usuario.is_staff}
            marcarResolvidoCallback={this.marcarResolvido}
          />
          <IniciarAtendimentoButton
            resolvido={resolvido}
            atendido={datahora_atendimento ? true : false}
            user_is_staff={usuario.is_staff}
            onClickHandle={this.iniciarAtendimento}
          />
          <EncerrarAtendimentoButton
            resolvido={resolvido}
            atendido={datahora_atendimento ? true : false}
            encerrado={datahora_encerramento ? true : false}
            user_is_staff={usuario.is_staff}
            onClickHandle={this.encerrarAtendimento}
          />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  ordensServico: state.ordensServico.ordensServico,
  usuario: state.autenticacao.user,
  isLoading: state.ordensServico.isLoading
});

const mapDispatchToProps = {
  marcarResolvidoOrdemServico,
  atenderOrdemServico,
  encerrarOrdemServico
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrdemServicoDetail);
