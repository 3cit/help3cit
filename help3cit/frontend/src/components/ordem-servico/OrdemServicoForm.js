import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { abrirOrdemServico } from "../../redux/actions/ordem-servico";

export class OrdemServicoForm extends Component {
  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
    ordensServico: PropTypes.array.isRequired,
    abrirOrdemServico: PropTypes.func.isRequired
  };

  state = {
    assunto: "",
    produto_servico: "",
    resumo: "",
    detalhe_problema: ""
  };

  handleChange = evt => {
    this.setState({ [evt.target.name]: evt.target.value });
  };

  handleSubmit = evt => {
    evt.preventDefault();
    const { assunto, produto_servico, resumo, detalhe_problema } = this.state;
    const { abrirOrdemServico, history } = this.props;
    abrirOrdemServico(
      {
        assunto,
        produto_servico,
        resumo,
        detalhe_problema
      },
      history
    );
  };

  render() {
    const { assunto, produto_servico, resumo, detalhe_problema } = this.state;
    const { isLoading } = this.props;
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="field">
          <label className="label">Produto</label>
          <div className="control">
            <div className="select">
              <select
                className="input"
                name="produto_servico"
                value={produto_servico}
                disabled={isLoading}
                onChange={this.handleChange}
              >
                <option>Selecione uma opção...</option>
                <option value="sige">SiGe</option>
                <option value="suporte">Suporte técnico/reparo</option>
                <option value="outro">Outro</option>
              </select>
            </div>
          </div>
          <div className="help">assunto da mensagem abaixo</div>
        </div>
        <div className="field">
          <label className="label">Assunto</label>
          <div className="control">
            <div className="select">
              <select
                className="input"
                name="assunto"
                value={assunto}
                disabled={isLoading}
                onChange={this.handleChange}
              >
                <option>Selecione uma opção...</option>
                <option value="bug">Relatar um problema</option>
                <option value="feedback">
                  Dizer o quanto está aproveitando...
                </option>
                <option value="dúvida">Sanar uma dúvida</option>
                <option value="reclamação">Fazer uma reclamação</option>
                <option value="sugestão">Sugerir algo...</option>
              </select>
            </div>
          </div>
          <div className="help">produto sobre o qual deseja tratar</div>
        </div>
        <div className="field">
          <label className="label">Resumo (seja bem sucinto...)</label>
          <p className="control">
            <input
              className="input"
              type="text"
              name="resumo"
              placeholder="Ex.: minha placa mãe queimou por culpa de vocês!1!"
              required
              value={resumo}
              disabled={isLoading}
              onChange={this.handleChange}
            />
          </p>
          <div className="help">
            uma frase que resuma bem o problema/questão
          </div>
        </div>
        <div className="field">
          <label className="label">Detalhe do problema/questão</label>
          <p className="control">
            <textarea
              className="textarea is-small"
              name="detalhe_problema"
              placeholder="Explique com detalhes ou descreva o que deseja..."
              required
              value={detalhe_problema}
              disabled={isLoading}
              onChange={this.handleChange}
            />
          </p>
          <div className="help">
            descrição mais detalhada do problema ou questão
          </div>
        </div>
        <div className="field is-grouped">
          <div className="control">
            <Link className="button" disabled={isLoading} to="/">
              Voltar
            </Link>
          </div>
          <div className="control">
            <button
              className={`button is-warning ${isLoading ? "is-loading" : ""}`}
              type="submit"
            >
              Abrir chamado
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.ordensServico.isLoading,
  ordensServico: state.ordensServico.ordensServico
});

const mapDispatchToProps = { abrirOrdemServico };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrdemServicoForm);
