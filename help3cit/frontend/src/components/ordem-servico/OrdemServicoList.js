import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getOrdensServico } from "../../redux/actions/ordem-servico";
import ProgressBar from "../layout/ProgressBar";

export class OrdemServicoList extends Component {
  static propTypes = {
    ordensServico: PropTypes.array.isRequired,
    getOrdensServico: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    match: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  componentDidMount() {
    this.props.getOrdensServico();
  }

  getApproxTime = dStr => {
    // TODO: Fzer retornar "há 1h, há 15min, agora mesmo.."
    const d = new Date(dStr);
    const dNow = new Date();
    let dStrFormatted = `${d.getHours()}:${d.getMinutes()}`;
    if (d.getDate() !== dNow.getDate())
      dStrFormatted = dStrFormatted.concat(
        ` em ${d.getDate()}/${d.getMonth() + 1}`
      );
    return dStrFormatted;
  };

  _goToOrdemServico = protocolo => () => {
    const { history, match } = this.props;
    history.push(`${match.path}${protocolo}`);
  };

  render() {
    const { isLoading, ordensServico } = this.props;

    if (isLoading) {
      return <ProgressBar />;
    }

    let ordemServicoSort = [];
    if (ordensServico)
      ordemServicoSort = this.props.ordensServico.sort((os1, os2) => {
        let d1 = new Date(os1.datahora_abertura);
        let d2 = new Date(os2.datahora_abertura);
        if (d1 > d2) {
          return -1;
        }
        if (d1 < d2) {
          return 1;
        }
        return 0;
      });

    return (
      <Fragment>
        {ordemServicoSort.length > 0 ? (
          <div className="limiter">
            <div className="container-table">
              <div className="wrap-table">
                <table className="table">
                  <thead>
                    <tr className="row header">
                      <th className="cell">Abertura</th>
                      <th className="cell">Sobre</th>
                      <th className="cell">Resumo</th>
                      <th className="cell">Resolvido?</th>
                      <th className="cell">Atendente</th>
                    </tr>
                  </thead>
                  <tbody>
                    {ordemServicoSort.map(os => (
                      <tr
                        key={os.id}
                        className="row selectable"
                        onClick={this._goToOrdemServico(os.protocolo)}
                      >
                        <td
                          className="cell"
                          data-column="Abertura"
                          data-title="Data da abertura"
                        >
                          {this.getApproxTime(os.datahora_abertura)}
                        </td>
                        <td className="cell">{os.produto_servico}</td>
                        <td className="cell">
                          [{os.assunto.toUpperCase()}] {os.resumo}
                        </td>
                        <td className="cell">
                          {os.resolvido ? (
                            <span className="has-text-success">Sim</span>
                          ) : (
                            <span className="has-text-danger">Não</span>
                          )}
                        </td>
                        <td className="cell">
                          {(os.datahora_atendimento && os.atendente) || "---"}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        ) : (
          <div>Não existem solicitações realizadas.</div>
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  ordensServico: state.ordensServico.ordensServico,
  isLoading: state.ordensServico.isLoading
});

const mapDispatchToProps = {
  getOrdensServico /*,
  getOrdemServico*/
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrdemServicoList);
