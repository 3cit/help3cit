import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

const PrivateRoute = ({ component: Component, autenticacao, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        if (autenticacao.isLoading) {
          return <div>Carregando...</div>;
        } else if (!autenticacao.isAuthenticated) {
          return <Redirect to="/" />;
        } else {
          return <Component {...props} />;
        }
      }}
    />
  );
};

const mapStateToProps = state => ({
  autenticacao: state.autenticacao
});

export default connect(mapStateToProps)(PrivateRoute);
