import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { alterUser } from "../../redux/actions/autenticacao";

class AlteracaoDadosForm extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired
  };

  state = {
    username: "",
    first_name: "",
    last_name: "",
    email: "",
    isSubmitting: false
  };

  componentDidMount() {
    //this.props.autenticacao.getUser()
    const { username, first_name, last_name, email } = this.props.user;
    this.setState({
      username: username,
      first_name: first_name,
      last_name: last_name,
      email: email
    });
  }

  handleChange = e => {
    this.setState({ [e.currentTarget.name]: e.currentTarget.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ isSubmitting: true });
    const { first_name, last_name, username, email } = this.state;
    this.props.alterUser({
      first_name,
      last_name,
      username,
      email
    });
    this.setState({ isSubmitting: true });
  };

  render() {
    const { first_name, last_name, username, email, isSubmitting } = this.state;
    return (
      <Fragment>
        <h2 className="title is-2">Alterando {first_name || username}</h2>
        <div className="content">
          <ul>
            <li>
              Ao alterar as informações de sua conta, esteja ciente de que os
              dados estão corretos e foram devidamente preenchidos.
            </li>
            <li>
              Se seu e-mail estiver incorreto, não será possível entrar em
              contato com prezado cliente, o que poderá invalidar sua
              reclamação.
            </li>
          </ul>
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Nome</label>
            </div>
            <div className="field-body">
              <div className="field">
                <p className="control">
                  <input
                    className="input"
                    type="text"
                    name="first_name"
                    placeholder="Ex.: José"
                    disabled={isSubmitting}
                    autoFocus
                    required
                    value={first_name}
                    onChange={this.handleChange}
                  />
                </p>
                <div className="help">primeiro nome para contato</div>
              </div>
              <div className="field">
                <p className="control">
                  <input
                    className="input"
                    type="text"
                    name="last_name"
                    placeholder="Ex.: Couves"
                    value={last_name}
                    disabled={isSubmitting}
                    onChange={this.handleChange}
                  />
                </p>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Usuário</label>
            </div>
            <div className="field-body">
              <div className="field">
                <p className="control">
                  <input
                    className="input"
                    type="text"
                    name="username"
                    placeholder="Ex.: jose123"
                    required
                    value={username}
                    disabled={isSubmitting}
                    onChange={this.handleChange}
                  />
                </p>
                <div className="help">
                  nome de usuário utilizado para acessar esta aplicação
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label">E-mail</div>
            <div className="field-body">
              <div className="field">
                <p className="control is-expanded has-icons-left">
                  <input
                    className="input"
                    type="email"
                    name="email"
                    placeholder="exemplo@email.com.br"
                    disabled={isSubmitting}
                    required
                    value={email}
                    onChange={this.handleChange}
                  />
                  <span className="icon is-small is-left">
                    <i className="fas fa-envelope" />
                  </span>
                </p>
                <div className="help">E-mail para contato</div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label" />
            <div className="field-body">
              <div className="field is-grouped">
                <div className="control">
                  <Link className="button" to="/">
                    Voltar
                  </Link>
                </div>
                <div className="control">
                  <button
                    className={`button is-warning ${
                      isSubmitting ? "is-loading" : ""
                    }`}
                    type="submit"
                  >
                    Alterar dados
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.autenticacao.user
});

const mapDispatchToProps = { alterUser };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AlteracaoDadosForm);
