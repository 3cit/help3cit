import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registerUser } from "../../redux/actions/autenticacao";
import { Redirect, Link } from "react-router-dom";

class RegistroForm extends Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    registerUser: PropTypes.func.isRequired
  };

  state = {
    first_name: "",
    last_name: "",
    username: "",
    password1: "",
    password2: "",
    email: "",
    isSubmitting: false
  };

  handleChange = evt => {
    this.setState({ [evt.currentTarget.name]: evt.currentTarget.value });
  };

  handleSubmit = evt => {
    evt.preventDefault();
    const {
      first_name,
      last_name,
      username,
      password1,
      password2,
      email
    } = this.state;
    if (password1 !== password2) {
      alert("Senhas não conferem!");
      this.setState({
        password1: "",
        password2: ""
      });
    } else {
      this.setState({ isSubmitting: true });
      this.props.registerUser({
        first_name,
        last_name,
        username,
        password: password1,
        email
      });
      this.setState({ isSubmitting: false });
    }
  };

  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to="/" />;
    }

    const {
      first_name,
      last_name,
      username,
      password1,
      password2,
      email,
      isSubmitting
    } = this.state;

    return (
      <Fragment>
        <h2 className="title is-2">Criando nova conta</h2>
        <div className="content">
          <p>
            Atenção ao preencher os dados. Eles serão utilizados para mantermos
            o contato contigo.
          </p>
          <p>
            Verifique também se o e-mail informado está correto e se a senha
            possui pelo menos um número e um caractere especial.
          </p>
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Nome</label>
            </div>
            <div className="field-body">
              <div className="field">
                <p className="control">
                  <input
                    className="input"
                    type="text"
                    name="first_name"
                    placeholder="Ex.: José"
                    disabled={isSubmitting}
                    autoFocus
                    required
                    value={first_name}
                    onChange={this.handleChange}
                  />
                </p>
                <div className="help">primeiro nome para contato</div>
              </div>
              <div className="field">
                <p className="control">
                  <input
                    className="input"
                    type="text"
                    name="last_name"
                    placeholder="Ex.: Couves"
                    value={last_name}
                    disabled={isSubmitting}
                    onChange={this.handleChange}
                  />
                </p>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Usuário</label>
            </div>
            <div className="field-body">
              <div className="field">
                <p className="control">
                  <input
                    className="input"
                    type="text"
                    name="username"
                    placeholder="Ex.: jose123"
                    required
                    value={username}
                    disabled={isSubmitting}
                    onChange={this.handleChange}
                  />
                </p>
                <div className="help">
                  nome de usuário utilizado para acessar esta aplicação
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Senha</label>
            </div>
            <div className="field-body">
              <div className="field">
                <p className="control">
                  <input
                    className="input"
                    type="password"
                    name="password1"
                    placeholder="Ex.: *****"
                    disabled={isSubmitting}
                    required
                    value={password1}
                    onChange={this.handleChange}
                  />
                </p>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Confirmação da senha</label>
            </div>
            <div className="field-body">
              <div className="field">
                <p className="control">
                  <input
                    className="input"
                    type="password"
                    name="password2"
                    value={password2}
                    placeholder="A mesma do campo anterior"
                    disabled={isSubmitting}
                    required
                    onChange={this.handleChange}
                  />
                </p>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label">E-mail</div>
            <div className="field-body">
              <div className="field">
                <p className="control is-expanded has-icons-left">
                  <input
                    className="input"
                    type="email"
                    name="email"
                    placeholder="exemplo@email.com.br"
                    disabled={isSubmitting}
                    required
                    value={email}
                    onChange={this.handleChange}
                  />
                  <span className="icon is-small is-left">
                    <i className="fas fa-envelope" />
                  </span>
                </p>
                <div className="help">E-mail para contato</div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label" />
            <div className="field-body">
              <div className="field is-grouped">
                <div className="control">
                  <Link className="button" to="/acesso">
                    Voltar
                  </Link>
                </div>
                <div className="control">
                  <button
                    className={`button is-success ${
                      isSubmitting ? "is-loading" : ""
                    }`}
                    type="submit"
                  >
                    Criar conta
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.autenticacao.isAuthenticated
});

const mapDispatchToProps = { registerUser };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistroForm);
