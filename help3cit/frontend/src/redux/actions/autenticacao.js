import axios from "axios";
import {
  AUTHENTICADING_USER,
  AUTHENTICATED_USER,
  AUTHENTICATION_FAILED,
  LOGGED_IN_USER,
  LOGGED_OUT_USER,
  ALTER_USER,
  ALTER_USER_FAILED,
  ALTERING_USER
} from "./types";
import { extractErrors, errorMessage, successMessage } from "./messages";
import { getRequestHeader, getPathToApi } from "./utils";

export const authenticateUser = () => (dispatch, getState) => {
  dispatch({
    type: AUTHENTICADING_USER
  });

  const currState = getState();
  ///localStorage.getItem("__sg_u_tkn");
  const REQUEST_HEADER = getRequestHeader(currState.autenticacao.accessToken);
  //console.log(REQUEST_HEADER);
  const PATH = getPathToApi("user/");
  axios
    .get(PATH, REQUEST_HEADER)
    .then(response => {
      dispatch({
        type: AUTHENTICATED_USER,
        payload: response.data
      });
    })
    .catch(err => {
      //if (err.response)
      //  dispatch(extractErrors(err.response.data, err.response.status));
      console.error(err.response.data);
      dispatch({
        type: AUTHENTICATION_FAILED
      });
    });
};

export const logUserIn = userCredentials => dispatch => {
  const { username, password } = userCredentials;
  const body = JSON.stringify({ username, password });
  const REQUEST_HEADER = getRequestHeader();
  const PATH = getPathToApi("login/");
  axios
    .post(PATH, body, REQUEST_HEADER)
    .then(response => {
      dispatch({
        type: LOGGED_IN_USER,
        payload: response.data
      });
    })
    .catch(err => {
      if (err.response) {
        dispatch(extractErrors(err.response.data, err.response.status));
      } else {
        dispatch(
          errorMessage(
            "Falha ao verificar ação: Usuário não possui requisitos necessários para realizar a ação."
          )
        );
      }
      dispatch({
        type: AUTHENTICATION_FAILED
      });
    });
};

export const logUserOut = () => (dispatch, getState) => {
  dispatch({
    type: AUTHENTICADING_USER
  });

  const REQUEST_HEADER = getRequestHeader(getState().autenticacao.accessToken);
  const PATH = getPathToApi("logout/");

  axios
    .post(PATH, null, REQUEST_HEADER)
    .then(() => {
      dispatch(successMessage("Até logo!"));
      dispatch({
        type: LOGGED_OUT_USER
      });
    })
    .catch(() => dispatch(errorMessage("O usuário já se encontra deslogado.")));
};

export const registerUser = ({
  first_name,
  last_name,
  username,
  password,
  email
}) => dispatch => {
  const REQUEST_HEADER = getRequestHeader();
  const PATH_TO_REGISTER = getPathToApi("users/novo/");
  const user = JSON.stringify({
    first_name,
    last_name,
    username,
    password,
    email
  });
  axios
    .post(PATH_TO_REGISTER, user, REQUEST_HEADER)
    .then(() => {
      dispatch({
        type: AUTHENTICADING_USER
      });
      const userCredentials = JSON.stringify({ username, password });
      const PATH_TO_LOGIN = getPathToApi("login/");
      axios
        .post(PATH_TO_LOGIN, userCredentials, REQUEST_HEADER)
        .then(response => {
          dispatch({
            type: LOGGED_IN_USER,
            payload: response.data
          });
        })
        .catch(err => {
          dispatch(
            successMessage(
              "Conta criada com sucesso. Insira seus dados para fazer o login."
            )
          );
        });
    })
    .catch(err => {
      if (err.response.data)
        dispatch(extractErrors(err.response.data, err.response.status));
      dispatch({ type: AUTHENTICATION_FAILED });
    });
};

export const alterUser = ({ first_name, last_name, username, email }) => (
  dispatch,
  getState
) => {
  let currentState = getState();
  const REQUEST_HEADER = getRequestHeader(
    currentState.autenticacao.accessToken
  );
  const PATH = getPathToApi("user/");
  const user = {
    username,
    first_name,
    last_name,
    email
  };

  dispatch({
    type: ALTERING_USER
  });

  axios
    .put(
      PATH,
      {
        username,
        first_name,
        last_name,
        email
      },
      REQUEST_HEADER
    )
    .then(response => {
      dispatch(successMessage("Dados salvos"));
      dispatch({
        type: ALTER_USER,
        payload: response.data
      });
    })
    .catch(err => {
      console.log(err.response.data);
      if (err.response.data)
        dispatch(extractErrors(err.response.data, err.response.status));
      dispatch(
        errorMessage("Não foi possível atualizar usuário. Tente novamente.")
      );
      dispatch({ type: ALTER_USER_FAILED });
    });
};
