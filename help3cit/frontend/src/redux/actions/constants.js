// Request setting header
export const REQUEST_HEADER = {
  headers: {
    "Content-Type": "application/json; charset=utf-8",
    Accept: "application/json"
    //Authorization: "Token "
  }
};

export const API_PREFIX = "api/beta/";
