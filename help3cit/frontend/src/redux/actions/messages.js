import { types } from "react-alert";
import {
  CREATE_MESSAGE,
  EXTRACT_ERRORS,
  CREATE_SUCCESS_MESSAGE,
  CREATE_ERROR_MESSAGE
} from "./types";

export const createMessage = messageText => {
  return {
    type: CREATE_MESSAGE,
    payload: messageText
  };
};

export const successMessage = messageText => {
  return {
    type: CREATE_SUCCESS_MESSAGE,
    payload: messageText
  };
};

export const errorMessage = messageText => {
  return {
    type: CREATE_ERROR_MESSAGE,
    payload: messageText
  };
};

export const extractErrors = (errRespData, status) => {
  const errorMessage = {
    level: types.ERROR,
    message: extractErrorResponseData(errRespData, status),
    status
  };
  return {
    type: EXTRACT_ERRORS,
    payload: errorMessage
  };
};

const extractErrorResponseData = (responseData, status = 400) => {
  let messageText = "";
  for (let key in responseData) {
    messageText +=
      status >= 200 && status < 300
        ? "S" // success message
        : status >= 400 && status < 500
        ? "E" // error message
        : "M"; // common message, internal server error, et cetera
    messageText += `${
      status > 100 ? status : status > 10 ? "0" + status : "00" + status
    }: `;
    if (typeof responseData[key] !== "string" && responseData[key].length > 0) {
      for (let errStr of responseData[key]) {
        messageText = `\n${errStr};`;
      }
    } else {
      messageText += `${responseData[key]}\n`;
    }
  }
  return messageText;
};
