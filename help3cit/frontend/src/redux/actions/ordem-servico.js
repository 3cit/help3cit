import axios from "axios";

import {
  CREATE_ORDEM_SERVICO,
  GET_ORDENS_SERVICO,
  MARK_AS_SOLVED,
  CREATING_ORDEM_SERVICO,
  GETTING_ORDENS_SERVICO,
  MARKING_AS_SOLVED,
  CREATE_ORDEM_SERVICO_FAIL,
  GET_ORDEM_SERVICO_FAIL,
  MARK_AS_SOLVED_FAIL,
  ATTENDING_ORDEM_SERVICO,
  ATTEND_ORDEM_SERVICO,
  ATTEND_ORDEM_SERVICO_FAIL,
  CLOSE_ORDEM_SERVICO,
  CLOSE_ORDEM_SERVICO_FAIL,
  CLOSING_ORDEM_SERVICO
} from "./types";
import { getRequestHeader, getPathToApi } from "./utils";
import { successMessage, extractErrors } from "../actions/messages";

// Adiciona ordem de serviço
export const abrirOrdemServico = (
  { assunto, produto_servico, resumo, detalhe_problema },
  history
) => (dispatch, getState) => {
  dispatch({
    type: CREATING_ORDEM_SERVICO
  });

  const REQUEST_HEADER = getRequestHeader(getState().autenticacao.accessToken);
  const PATH = getPathToApi("os/abrir/");
  const ordemServico = {
    assunto,
    resumo,
    produto_servico,
    detalhe_problema
  };
  axios
    .post(PATH, ordemServico, REQUEST_HEADER)
    .then(response => {
      dispatch({
        type: CREATE_ORDEM_SERVICO,
        payload: response.data
      });
      dispatch(successMessage("OS aberta com sucesso."));
      // Se tiver criado uma nova OS, redireciona o usuário...
      history.push("/os/");
    })
    .catch(err => {
      dispatch(extractErrors(err.response.data, err.response.status));
      dispatch({
        type: CREATE_ORDEM_SERVICO_FAIL
      });
    });
};

// GET ordens de serviço
export const getOrdensServico = () => (dispatch, getState) => {
  dispatch({
    type: GETTING_ORDENS_SERVICO
  });
  const REQUEST_HEADER = getRequestHeader(getState().autenticacao.accessToken);
  const PATH = getPathToApi("os/");
  axios
    .get(PATH, REQUEST_HEADER)
    .then(response => {
      dispatch({
        type: GET_ORDENS_SERVICO,
        payload: response.data
      });
    })
    .catch(err => {
      dispatch(extractErrors(err.response.data, err.response.status));
      dispatch({
        type: GET_ORDEM_SERVICO_FAIL
      });
    });
};

// GET ordem de serviço
export const marcarResolvidoOrdemServico = id => (dispatch, getState) => {
  dispatch({
    type: MARKING_AS_SOLVED
  });
  const REQUEST_HEADER = getRequestHeader(getState().autenticacao.accessToken);
  const PATH = getPathToApi(`os/${id}/mark-resolvido/`);
  axios
    .patch(PATH, null, REQUEST_HEADER)
    .then(response => {
      dispatch({
        type: MARK_AS_SOLVED,
        payload: response.data
      });
    })
    .catch(err => {
      dispatch(extractErrors(err.response.data, err.response.status));
      dispatch({
        type: MARK_AS_SOLVED_FAIL
      });
    });
};

export const atenderOrdemServico = id => (dispatch, getState) => {
  dispatch({
    type: ATTENDING_ORDEM_SERVICO
  });
  const REQUEST_HEADER = getRequestHeader(getState().autenticacao.accessToken);
  const PATH = getPathToApi(`os/${id}/atender/`);
  axios
    .patch(PATH, null, REQUEST_HEADER)
    .then(response => {
      dispatch({
        type: ATTEND_ORDEM_SERVICO,
        payload: response.data
      });
    })
    .catch(err => {
      dispatch(extractErrors(err.response.data, err.response.status));
      dispatch({
        type: ATTEND_ORDEM_SERVICO_FAIL
      });
    });
};

export const encerrarOrdemServico = id => (dispatch, getState) => {
  dispatch({
    type: CLOSING_ORDEM_SERVICO
  });
  const REQUEST_HEADER = getRequestHeader(getState().autenticacao.accessToken);
  const PATH = getPathToApi(`os/${id}/encerrar/`);
  axios
    .patch(PATH, null, REQUEST_HEADER)
    .then(response => {
      dispatch({
        type: CLOSE_ORDEM_SERVICO,
        payload: response.data
      });
    })
    .catch(err => {
      dispatch(extractErrors(err.response.data, err.response.status));
      dispatch({
        type: CLOSE_ORDEM_SERVICO_FAIL
      });
    });
};
