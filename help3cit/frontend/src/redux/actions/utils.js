import { REQUEST_HEADER, API_PREFIX } from "./constants";

export const getPathToApi = path => {
  const { protocol, host } = document.location;
  return `${protocol}//${host}/${API_PREFIX}${path}`;
};

export const getRequestHeader = (token = null) => {
  //console.log("Token " + token);
  if (Boolean(token)) {
    return {
      headers: {
        ...REQUEST_HEADER,
        Authorization: "Token " + token
      }
    };
  }
  return REQUEST_HEADER;
};
