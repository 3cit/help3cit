import {
  AUTHENTICATE_USER,
  AUTHENTICADING_USER,
  AUTHENTICATED_USER,
  AUTHENTICATION_FAILED,
  LOGGED_IN_USER,
  LOGGED_OUT_USER,
  ALTER_USER,
  ALTERING_USER,
  ALTER_USER_FAILED
} from "../actions/types";
import { getAccessToken, setAccessToken, removeAccessToken } from "../../utils";

const initialState = {
  user: null,
  // TODO: Read https://dev.to/rdegges/please-stop-using-local-storage-1i04
  accessToken: getAccessToken() || null,
  isAuthenticated: false,
  isLoading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case AUTHENTICADING_USER:
      return {
        ...state,
        isLoading: true
      };
    case AUTHENTICATED_USER:
      return {
        ...state,
        isLoading: false,
        user: action.payload,
        isAuthenticated: true
      };
    case LOGGED_IN_USER:
      const { token, ...user } = action.payload;
      setAccessToken(token);
      return {
        ...state,
        isLoading: false,
        accessToken: token,
        user: user,
        isAuthenticated: true
      };
    case AUTHENTICATION_FAILED:
    case LOGGED_OUT_USER:
      removeAccessToken();
      return {
        ...state,
        isLoading: false,
        accessToken: null,
        user: null,
        isAuthenticated: false
      };
    case ALTER_USER:
      return {
        ...state,
        isLoading: false,
        user: action.payload
      };
    case ALTERING_USER:
      return {
        ...state,
        isLoading: true
      };
    case ALTER_USER_FAILED:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
}
