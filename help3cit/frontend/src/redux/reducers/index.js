import { combineReducers } from "redux";
import ordensServico from "./ordem-servico";
import messages from "./messages";
import autenticacao from "./autenticacao";

export default combineReducers({
  autenticacao,
  ordensServico,
  messages
});
