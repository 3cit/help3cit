import { types } from "react-alert";
import {
  CREATE_MESSAGE,
  CREATE_SUCCESS_MESSAGE,
  CREATE_ERROR_MESSAGE,
  EXTRACT_ERRORS
} from "../actions/types";

const initialState = {
  level: "info",
  message: "",
  status: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CREATE_MESSAGE:
      return {
        ...state,
        level: action.payload.level ? action.payload.level : types.INFO,
        message: action.payload.message
      };
    case CREATE_SUCCESS_MESSAGE:
      return {
        ...state,
        level: types.SUCCESS,
        message: action.payload
      };
    case CREATE_ERROR_MESSAGE:
      return {
        ...state,
        level: types.ERROR,
        message: action.payload
      };
    case EXTRACT_ERRORS:
      return {
        level: types.ERROR,
        message: action.payload.message,
        status: action.payload.status
      };
    default:
      return state;
  }
}
