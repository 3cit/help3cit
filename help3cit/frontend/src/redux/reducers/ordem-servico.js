import {
  GET_ORDENS_SERVICO,
  CREATE_ORDEM_SERVICO,
  MARK_AS_SOLVED,
  CREATING_ORDEM_SERVICO,
  GETTING_ORDENS_SERVICO,
  MARKING_AS_SOLVED,
  GET_ORDEM_SERVICO_FAIL,
  MARK_AS_SOLVED_FAIL,
  CREATE_ORDEM_SERVICO_FAIL,
  ATTENDING_ORDEM_SERVICO,
  ATTEND_ORDEM_SERVICO,
  ATTEND_ORDEM_SERVICO_FAIL,
  CLOSE_ORDEM_SERVICO,
  CLOSE_ORDEM_SERVICO_FAIL,
  CLOSING_ORDEM_SERVICO
} from "../actions/types";

const initialState = {
  isLoading: false,
  ordensServico: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CREATING_ORDEM_SERVICO:
    case GETTING_ORDENS_SERVICO:
    case ATTENDING_ORDEM_SERVICO:
    case CLOSING_ORDEM_SERVICO:
    case MARKING_AS_SOLVED:
      return {
        ...state,
        isLoading: true
      };
    case CREATE_ORDEM_SERVICO:
      return {
        ...state,
        ordensServico: [...state.ordensServico, action.payload],
        isLoading: false
      };
    case GET_ORDEM_SERVICO_FAIL:
    case MARK_AS_SOLVED_FAIL:
    case CREATE_ORDEM_SERVICO_FAIL:
    case ATTEND_ORDEM_SERVICO_FAIL:
    case CLOSE_ORDEM_SERVICO_FAIL:
      return {
        ...state,
        isLoading: false
      };
    case GET_ORDENS_SERVICO:
      return {
        ...state,
        ordensServico: action.payload,
        isLoading: false
      };
    case MARK_AS_SOLVED:
    case CLOSE_ORDEM_SERVICO:
    case ATTEND_ORDEM_SERVICO:
      return {
        ...state,
        ordensServico: [
          ...state.ordensServico.filter(
            os => os.protocolo !== action.payload.protocolo
          ),
          action.payload
        ],
        isLoading: false
      };
    default:
      return state;
  }
}
