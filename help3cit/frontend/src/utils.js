// UTILS
const TOKEN_FIELD_NAME = "u3ctk"; // user access token

/**
 * A function to set a cookie easily.
 * Create by W3C.
 * @see More at https://www.w3schools.com/js/js_cookies.asp
 * @param {String} cname
 */
export const getCookie = cname => {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};

/**
 * A function to set a cookie easily.
 * Create by W3C.
 * @see More at https://www.w3schools.com/js/js_cookies.asp
 * @param {String} cname
 * @param {String} cvalue
 * @param {Number} exdays
 */
export const setCookie = (cname, cvalue, exdays) => {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = `expires=${d.toUTCString()}`;
  //document.cookie = `${cname}=${cvalue};${expires};path=/;SameSite=strict;secure=${onlyHttps}`;
  document.cookie = `${cname}=${cvalue}; ${expires}; path=/`;
};

export const getAccessToken = () => {
  return getCookie(TOKEN_FIELD_NAME);
};

export const setAccessToken = token => {
  setCookie(TOKEN_FIELD_NAME, token, 0.5); // 12 hours
};

export const removeAccessToken = () => {
  setCookie(TOKEN_FIELD_NAME, "", 0);
};
