from django.test import TestCase


class FrontendTests(TestCase):
    def test_carregamento_index(self):
        response = self.client.get('/app/')
        self.assertEqual(response.status_code, 200,
                         'Falha ao acessar página do app')
        self.assertIn('HELP! 3cIT', response.content.decode('utf8'),
                      'Falha ao encontrar "HELP! 3cIT" no título da resposta')

    # TODO: Continuar testes de frontend com o selenium
