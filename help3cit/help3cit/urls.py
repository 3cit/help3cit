"""Help! 3cIT URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from rest_framework.documentation import include_docs_urls

apipatterns = [
    path('', include('autenticacao.api_urls')),
    path('', include('ordem_servico.urls')),
]

urlpatterns = [
    re_path(r'^restrito/', admin.site.urls),
    re_path(r'^api/beta/', include(apipatterns)),
    re_path(r'^app/', include('frontend.urls')),
    re_path(
        r'^documentacao/',
        include_docs_urls(title="Help, 3cIT!.doc")),
]
