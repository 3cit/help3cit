from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils import timezone

PRODUTO_SERVICO_SIGE = 'sige'
PRODUTO_SERVICO_SUPORTE_TECNICO = 'suporte'
PRODUTO_SERVICO_OUTRO = 'outro'
PRODUTOS_SERVICOS_CHOICES = (
    (PRODUTO_SERVICO_SIGE, 'SiGe'),
    (PRODUTO_SERVICO_SUPORTE_TECNICO, 'Suporte técnico'),
    (PRODUTO_SERVICO_OUTRO, 'Outro'))

ASSUNTO_CONTATO_DUVIDA = 'dúvida'
ASSUNTO_CONTATO_BUG = 'bug'
ASSUNTO_CONTATO_RECLAMACAO = 'reclamação'
ASSUNTO_CONTATO_SUGESTAO = 'sugestão'
ASSUNTO_CONTATO_FEEDBACK = 'feedback'
ASSUNTOS_CONTATO_CHOICES = (
    (ASSUNTO_CONTATO_BUG, 'Relatar um problema'),
    (ASSUNTO_CONTATO_DUVIDA, 'Tirar uma dúvida'),
    (ASSUNTO_CONTATO_RECLAMACAO, 'Fazer uma reclamação'),
    (ASSUNTO_CONTATO_SUGESTAO, 'Sugerir algo'),
    (ASSUNTO_CONTATO_FEEDBACK, 'Nos dizer como está aproveitando o SiGe')
)

User = get_user_model()


class OrdemServicoModel(models.Model):
    protocolo = models.CharField(unique=True,
                                 max_length=20)  # 00000000000000MMYYYY
    datahora_abertura = models.DateTimeField(auto_now_add=True)
    produto_servico = models.CharField(max_length=20,
                                       choices=PRODUTOS_SERVICOS_CHOICES)
    assunto = models.CharField(max_length=15, choices=ASSUNTOS_CONTATO_CHOICES)
    resumo = models.CharField(max_length=120)
    detalhe_problema = models.TextField(max_length=1000)
    anexo = models.FileField(blank=True, null=True, upload_to='anexos-os/')
    #                         content_types=['image/jpg', 'image/jpeg',
    #                                        'image/png', 'video/mp4', 'text/plain'])
    requerente_id = models.PositiveIntegerField()
    atendente_id = models.PositiveIntegerField(blank=True, null=True)
    datahora_atendimento = models.DateTimeField(blank=True, null=True)
    resolvido = models.BooleanField(default=False)
    datahora_encerramento = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'help3c_ordem_servico'
        verbose_name = "Ordem de serviço"
        verbose_name_plural = "Ordens de serviço"

    def get_atendente_first_name(self):
        """
        Retorna o nome do atendente da ordem de serviço.
        """
        if not self.atendente_id:
            return None
        try:
            u = User.objects.get(pk=self.atendente_id)
        except ObjectDoesNotExist:
            return None
        return u.first_name

    def get_requerente_first_name(self):
        """
        Retorna o primeir onome do requerente (quem abriu a OS).
        """
        try:
            u = User.objects.get(pk=self.atendente_id)
        except ObjectDoesNotExist:
            return None
        return u.first_name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None:
            now = timezone.now()
            self.protocolo = "{:0>14}{:%m%Y}".format(
                OrdemServicoModel.objects.filter(datahora_abertura__month=now.month,
                                                 datahora_abertura__year=now.year).count() + 1,
                now)
            # Feedbacks e sugestões são apenas recebidos pela equipe 3cit, não
            # requerendo qualquer tipo de retorno.
            if self.assunto == ASSUNTO_CONTATO_FEEDBACK or self.assunto == ASSUNTO_CONTATO_SUGESTAO:
                self.resolvido = True
                self.datahora_encerramento = now
        return super().save(force_insert, force_update, using)

# class Resposta(models.Model):
#    texto = models.TextField()
#    ordem_servico
#    datahora_envio = models.DateTimeField(auto_now_add=True)
