from .constants import APP_NAME


class OrdemServicoRouter:

    def db_for_read(self, model, **hints):
        if model._meta.app_label == APP_NAME:
            return APP_NAME + '_db'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == APP_NAME:
            return APP_NAME + '_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Permite o relacionamento entre objetos se um deles forem pertencentes
        ao app.
        """
        if obj1._meta.app_label == APP_NAME and obj2._meta.app_label == APP_NAME:
            return True
        elif obj1._meta.app_label != APP_NAME and obj2._meta.app_label != APP_NAME:
            return None
        else:
            return False

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == APP_NAME:
            return db == APP_NAME + '_db'
        elif db == APP_NAME + '_db':  # outro app tentando gravar no db de OS..
            return False
        return None
