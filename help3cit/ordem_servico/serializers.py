import logging
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from rest_framework.serializers import ModelSerializer, ReadOnlyField

from autenticacao.serializers import UserSerializer
from thrcommons.utils import EmailSendingThread
from .models import OrdemServicoModel

logger = logging.getLogger('help3cit.ordem_servico')


class OrdemServicoSerializer(ModelSerializer):
    atendente = ReadOnlyField(source='get_atendente_first_name')
    requerente = ReadOnlyField(source='get_requerente_first_name')

    class Meta:
        model = OrdemServicoModel
        fields = ('id', 'protocolo', 'datahora_abertura', 'produto_servico', 'assunto',
                  'resumo', 'detalhe_problema', 'anexo', 'requerente', 'atendente',
                  'datahora_atendimento', 'resolvido', 'datahora_encerramento')
        extra_kwargs = {
            'protocolo': {
                'read_only': True
            },
            'atendente': {
                'read_only': True
            },
            'requerente': {
                'read_only': True
            },
            'datahora_atendimento': {
                'read_only': True
            },
            'datahora_encerramento': {
                'read_only': True
            },
            'resolvido': {
                'read_only': True
            }
        }

    def create(self, validated_data):
        """
        Cria uma intância de ordem de serviço e notifica a equipe 3cIT informando
        a abertura de uma nova demanda.
        """
        os_inst = super().create(validated_data)
        request = self._context['request']

        #
        logger.debug('A preparar e-mail para informar abertura da demanda #{} ...'.format(
            os_inst.protocolo, request.user.username))

        thrcit_contact = settings.DEFAULT_SAC_EMAIL
        email_config = dict(
            subject_text_file_name='ordem_servico/assunto_abertura_ordem_servico.txt',
            message_text_file_name='ordem_servico/abertura_ordem_servico.txt',
            recipient_list=[thrcit_contact],
            html_template_name='ordem_servico/abertura_ordem_servico.html',
            context_data={'user': request.user,
                          'ordem_servico': os_inst})

        # if bool(request.user.email):
        #    email_config.setdefault('from_email', request.user.email)
        #    if bool(request.user.first_name):
        #        email_config.setdefault('from_email_owner', request.user.email)

        email_sender = EmailSendingThread(**email_config)

        logger.info(
            'Iniciando envio de e-mail para {}...'.format(thrcit_contact))
        email_sender.start()

        return os_inst
