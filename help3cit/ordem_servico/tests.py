import json
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.test import TestCase
from ordem_servico.models import OrdemServicoModel
from rest_framework.test import APITestCase

User = get_user_model()

PREFIX_API = '/api/beta/'
PATH_LOGIN = '{}login/'.format(PREFIX_API)
PATH_ABERTURA_OS = '{}os/abrir/'.format(PREFIX_API)
PATH_ATENDIMENTO_OS = '{}os/{}/atender/'.format(PREFIX_API, '{pk}')
PATH_ENCERRAMENTO_OS = '{}os/{}/encerrar/'.format(PREFIX_API, '{pk}')
PATH_RESOLUCAO_OS = '{}os/{}/mark-resolvido/'.format(
    PREFIX_API, '{pk}')


class OrdemServicoTests(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_dict = dict(first_name='José', username='josecouves', password='jo@0zz23!!',
                             email='jose@couves.com.br')
        cls.superuser_dict = dict(first_name='Admin', username='admin', password='jo@0zz23!!',
                                  email='admin@exemplo.com.br')
        cls.user_instance = User.objects.create_user(**cls.user_dict)
        cls.superuser_instance = User.objects.create_superuser(
            **cls.superuser_dict)
        # cls.client.post('/api/beta/login/', {'username': user_dict['username'],
        # 'password': user_dict['password']})

    def test_criacao_ordem_servico(self):
        resp_user_login = self.client.post('{}login/'.format(PREFIX_API), {'username': self.user_dict['username'],
                                                                           'password': self.user_dict['password']}, 'json')
        self.assertEqual(resp_user_login.status_code, 200,
                         'Falha ao logar com o usuário')
        token = json.loads(resp_user_login.content, encoding='utf8')['token']

        os1_dict = dict(
            assunto='reclamação',
            produto_servico='suporte',
            resumo='meu teclado parou de funcionar!',
            detalhe_problema=(
                'Estava mexendo no meu computador, quando de repente, o meu'
                'teclado parou de funcionar. Exijo uma reparação urgente, pois'
                'é para isso que eu pago impostos! Isso é um absurdo..!'))
        self.client.credentials(HTTP_AUTHORIZATION='Token {}'.format(token))
        resp_criacao_os = self.client.post('{}os/abrir/'.format(PREFIX_API), os1_dict,
                                           'json')
        self.assertEqual(resp_criacao_os.status_code,
                         201, 'Falha ao criar nova OS')

        os1_from_db = self.user_instance.solicitacoes.filter().get()
        self.assertEqual(os1_dict['assunto'], os1_from_db.assunto,
                         'Valor de assunto diferente ao cadastrar no banco')
        self.assertEqual(os1_dict['resumo'], os1_from_db.resumo,
                         'Valor do campo resumo difere do esperado')
        # Testa a geração do protocolo com base nos registros já existentes
        self.assertEqual(os1_from_db.protocolo, "00000000000001{:%m%Y}".format(
            timezone.now()))

        os2_dict = dict(
            assunto='bug',
            produto_servico='outro',
            resumo='se 1 + 1 = 2, por que 2 * 0 = 0?',
            detalhe_problema=(
                'Uma dúvida matemática bastante séria que eu tenho é quanto '
                'as ordens dos fatores e a alteração destas nos produtos. Bem...'))
        resp_criacao_2da_os = self.client.post('{}os/abrir/'.format(PREFIX_API), os1_dict,
                                               'json')
        self.assertEqual(resp_criacao_2da_os.status_code, 201,
                         'Falha ao criar segunda OS')
        os2 = self.user_instance.solicitacoes.last()
        self.assertEqual(os2.protocolo, "00000000000002{:%m%Y}".format(
            timezone.now()))
        # Remove o token do cliente, afim de não poluir outros testes
        self.client.credentials()

    def test_fluxo_completo_ordem_servico(self):
        """
        Testa a criação, o atendimento, o encerramento de uma ordem de serviço
        através da 'declaração' de resolução por parte do usuário requerente e
        também o encerramento de uma ordem de serviço por parte do administrador.

        Os testes foram concentrados aqui afim de minimizar boilerplate na
        criação de ordens de serviço.
        """

        # Afim de não poluir os clientes, cria um para cada 'usuário'
        user_client = self.client_class()
        admin_client = self.client_class()

        # Se autentica como usuário e superusuário.
        # Primeiro, como usuário comum...
        credentials_dict = dict(username=self.user_dict['username'],
                                password=self.user_dict['password'])
        user_login_resp = user_client.post(PATH_LOGIN, credentials_dict,
                                           "json")
        self.assertEqual(user_login_resp.status_code, 200,
                         'Falha ao realizar login do usuário')
        user_data = json.loads(user_login_resp.content, encoding='utf8')
        user_client.credentials(
            HTTP_AUTHORIZATION='Token {}'.format(user_data['token']))
        # ... depois, como superusuário.
        credentials_dict['username'] = self.superuser_dict['username']
        credentials_dict['password'] = self.superuser_dict['password']
        admin_login_resp = admin_client.post(PATH_LOGIN, credentials_dict,
                                             "json")
        self.assertEqual(admin_login_resp.status_code, 200,
                         'Falha ao realizar login do superusuário')
        admin_data = json.loads(admin_login_resp.content, encoding='utf8')
        admin_client.credentials(
            HTTP_AUTHORIZATION='Token {}'.format(admin_data['token']))
        # Começa com o usuário criando uma nova OS
        ordem_servico_dict = {'assunto': "bug", 'produto_servico': "suporte",
                              'resumo': "não consigo recuperar minhas fotos do "
                              "HD externo. E agr???? O q fazr??",
                              'detalhe_problema': "Desde que vocês vieram aqui, "
                              "não consigo mais recuperar os dados do meu HD externo,."
                              "Tp, acho iso a maio falta de repeito com o consumido, "
                              "sabe? tipo quero o concerto ou meu dinheiro de voltaa!11"}
        resp_creat_os = user_client.post(PATH_ABERTURA_OS, ordem_servico_dict,
                                         "json")
        self.assertEqual(resp_creat_os.status_code, 201,
                         'Falha ao abrir nova ordem de serviço')
        os_data = json.loads(resp_creat_os.content, encoding='utf8')
        # Tenta iniciar um atendimento como usuário (esperado Unauthorized)
        resp_try_atend_as_usr = user_client.patch(
            PATH_ATENDIMENTO_OS.format(pk=os_data['id']))
        self.assertEqual(resp_try_atend_as_usr.status_code, 403,
                         'O usuário atendeu ao próprio chamado ou erro desconhecido')
        # Tenta inicar um atendimento a um ID inválido (esperado Not Found)
        invalid_pk = int(os_data['id']) + 7239847892374  # random number
        resp_try_atend_inv_pk = admin_client.patch(
            PATH_ATENDIMENTO_OS.format(pk=invalid_pk))
        self.assertEqual(resp_try_atend_inv_pk.status_code, 404,
                         'Um erro inesperado aconteceu')
        # Realiza o atendimento corretamente
        resp_atend_os = admin_client.patch(
            PATH_ATENDIMENTO_OS.format(pk=os_data['id']))
        self.assertEqual(resp_atend_os.status_code, 200,
                         'Falha ao iniciar atendimento chamado')

        # Agora testa o encerramento de uma OS assim que o usuário marca o próprio
        # chamado como 'resolvido'
        r_try_mark_res_inv_pk = user_client.patch(
            PATH_RESOLUCAO_OS.format(pk=invalid_pk))
        self.assertEqual(resp_try_atend_inv_pk.status_code, 404,
                         'Um erro inesperado aconteceu')
        # Tenta declarar 'encerrado' como 'admin'
        r_try_mark_res_as_admin = admin_client.patch(
            PATH_RESOLUCAO_OS.format(pk=os_data['id']))
        self.assertEqual(r_try_mark_res_as_admin.status_code, 404,
                         'Admin marcou OS do usuário como "resolvido"')
        resp_mark_resolvido = user_client.patch(
            PATH_RESOLUCAO_OS.format(pk=os_data['id']))
        self.assertEqual(resp_mark_resolvido.status_code, 200,
                         'Falha ao marcar como "resolvido"')
        os_resolvida_data = json.loads(
            resp_mark_resolvido.content, encoding='utf8')
        self.assertTrue(os_resolvida_data['resolvido'],
                        'Falha ao resolver ordem de serviço')
        self.assertIsNotNone(os_resolvida_data['datahora_encerramento'],
                             'Ordem de serviço resolvida sem data de encerramento')

        # Cadastra outra OS, dessa vez para encerramento e só depois a marcação
        # como resolvido pelo usuário
        ordem_servico_2_dict = dict(assunto='dúvida', produto_servico='sige',
                                    resumo='como eu altero uma informação relativo ao SIAPE em um consignado?',
                                    detalhe_problema='o problema está no assunto. Grato.')
        resp_creat_os2 = user_client.post(PATH_ABERTURA_OS, ordem_servico_2_dict,
                                          "json")
        self.assertEqual(resp_creat_os2.status_code, 201,
                         'Falha ao abrir a segunda ordem de serviço')
        os2_data = json.loads(resp_creat_os2.content, encoding='utf8')
        resp_atend_os2 = admin_client.patch(
            PATH_ATENDIMENTO_OS.format(pk=os2_data['id']))
        self.assertEqual(resp_atend_os2.status_code, 200,
                         'Falha ao iniciar atendimento chamado')
        resp_encer_os2 = admin_client.patch(
            PATH_ENCERRAMENTO_OS.format(pk=os2_data['id']))
        self.assertEqual(resp_encer_os2.status_code, 200,
                         'Falha ao encerrar ordem de serviço 2')
        os2_encerrado_data = json.loads(
            resp_encer_os2.content, encoding='utf8')
        self.assertIsNotNone(os2_encerrado_data['datahora_encerramento'])
        self.assertFalse(os2_encerrado_data['resolvido'],
                         'A ordem de serviço já foi marcada como "resolvida"')
        resp_mark_os2_resolvido = user_client.patch(
            PATH_RESOLUCAO_OS.format(pk=os2_data['id']))
        self.assertEqual(resp_mark_os2_resolvido.status_code, 200,
                         'Falha ao marcar segunda OS como "resolvido"')
        os2_resolv_data = json.loads(resp_mark_os2_resolvido.content,
                                     encoding='utf8')
        self.assertTrue(os2_resolv_data['resolvido'],
                        'Falha ao resolver ordem de serviço 2')
        self.assertEqual(os2_resolv_data['datahora_encerramento'],
                         os2_encerrado_data['datahora_encerramento'],
                         'Data-hora de encerramento alterado indevidamente')
