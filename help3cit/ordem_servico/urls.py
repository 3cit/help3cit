from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import (CreateOrdemServicoAPIView, ListOrdensServicoAPIView,
                    RetrieveUpdateOrdemServicoAPIView, atender_ordem_servico,
                    set_resolvido_ordem_servico, encerrar_ordem_servico)

urlpatterns = [
    path('os/abrir/', CreateOrdemServicoAPIView.as_view()),
    path('os/', ListOrdensServicoAPIView.as_view()),
    path('os/<int:pk>/', RetrieveUpdateOrdemServicoAPIView.as_view()),
    path('os/<int:pk>/atender/', atender_ordem_servico),
    path('os/<int:pk>/encerrar/', encerrar_ordem_servico),
    path('os/<int:pk>/mark-resolvido/', set_resolvido_ordem_servico),
]

urlpatterns = format_suffix_patterns(urlpatterns)
