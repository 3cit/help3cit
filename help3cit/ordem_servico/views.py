from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from rest_framework import generics, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response

from comum.permissions import IsItSelfOrReadOnly
from .models import OrdemServicoModel
from .serializers import OrdemServicoSerializer

User = get_user_model()


class CreateOrdemServicoAPIView(generics.CreateAPIView):
    "Abre uma nova ordem de serviço, notificando a empresa por e-mail"
    permission_classes = (IsAuthenticated,)
    serializer_class = OrdemServicoSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data,
                                         context={'request': request})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED,
                        headers=headers)

    def perform_create(self, serializer):
        serializer.save(requerente_id=self.request.user.id)


class LimitOrdensServicoByUserMixin:
    """
    Mixin para limitação da listagem de ordens de serviço por usuário, caso este
    não seja um admin.
    """

    def get_queryset(self):
        queryset = self.queryset or None
        if self.request.user.is_staff:
            queryset = OrdemServicoModel.objects.all()
        else:
            queryset = OrdemServicoModel.objects.filter(
                requerente_id=self.request.user.id)
        return queryset


class ListOrdensServicoAPIView(LimitOrdensServicoByUserMixin, generics.ListAPIView):
    """
    Lista as ordens de serviços existentes do usuário ou caso admin, de todos
    os usuários.

    Essa identificação é feita com base no token utilizado pelo usuário para acessar
    a este recurso.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = OrdemServicoSerializer


class RetrieveUpdateOrdemServicoAPIView(LimitOrdensServicoByUserMixin, generics.RetrieveUpdateAPIView):
    """
    get:
    Retorna detalhes da Ordem de Serviço.

    put:
    Atualiza uma ordem de serviço.

    patch:
    Altera um dado ou alguns campos da ordem de serviço (atendimento ou se resolvido,
    por exemplo).
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = OrdemServicoSerializer


# TODO: Unificar os três metodos abaixo, já que todas tem a mesma estrutura...
#OS_ACTIONS_LIST = ['atender', 'encerrar', 'mark-resolvido']

@api_view(['PATCH'])
@permission_classes((IsAuthenticated, IsAdminUser,))
def atender_ordem_servico(request, pk):
    """
    Atende uma ordem de serviço, identificando um usuário admin para a ordem de
    serviço identificada através de do número de id.
    """
    serializer_class = OrdemServicoSerializer
    os = None
    try:
        os = OrdemServicoModel.objects.get(pk=pk, atendente_id__isnull=True)
        os.atendente = request.user
        os.datahora_atendimento = timezone.now()
        os.save()
    except ObjectDoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serialized_os = serializer_class(os)
    return Response(data=serialized_os.data, status=status.HTTP_200_OK)


@api_view(['PATCH'])
@permission_classes((IsAuthenticated,))
def set_resolvido_ordem_servico(request, pk):
    """
    Declara uma ordem de serviço como 'resolvido', por parte do usuário requerente.
    """
    serializer_class = OrdemServicoSerializer
    os = None
    try:
        os = OrdemServicoModel.objects.get(pk=pk, requerente_id=request.user.id,
                                           resolvido=False)
        os.resolvido = True
        # Apenas o usuário poderá settar como resolvido as próprias OSs
        if os.datahora_encerramento is None:
            os.datahora_encerramento = timezone.now()
        os.save()
    except ObjectDoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serialized_os = serializer_class(os)
    return Response(data=serialized_os.data, status=status.HTTP_200_OK)


@api_view(['PATCH'])
@permission_classes((IsAuthenticated, IsAdminUser,))
def encerrar_ordem_servico(request, pk):
    """
    Define como encerrada uma ordem de serviço com base no usuário atendente
    e no número de id da OS.
    """
    serializer_class = OrdemServicoSerializer
    os = None
    try:
        os = OrdemServicoModel.objects.get(
            pk=pk, atendente_id=request.user.id)
        os.datahora_encerramento = timezone.now()
        os.save()
    except ObjectDoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serialized_os = serializer_class(os)
    return Response(data=serialized_os.data, status=status.HTTP_200_OK)
