import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="help3cit",
    author="3cit",
    author_email="3cit.sd@gmail.com",
    description="App para gerência das chamadas realizadas pelos clientes.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/3cit/help3cit",
    packages=setuptools.find_packages(),
    classifiers=(
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3",
        "Framework :: Django :: 2.2",
        "Natural Language :: Portuguese (Brazilian)",
        "Operating System :: OS Independent",
    ),
)
